module routing

go 1.18

require github.com/julienschmidt/httprouter v1.3.0 // direct

require github.com/gorilla/mux v1.8.0 // direct
