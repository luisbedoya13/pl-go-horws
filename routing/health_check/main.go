package main

import (
	"io"
	"log"
	"net/http"
	"time"
)

func healthCheck(rw http.ResponseWriter, r *http.Request) {
	currentTime := time.Now()
	io.WriteString(rw, currentTime.String())
}

func main() {
	http.HandleFunc("/health", healthCheck)
	log.Fatal(http.ListenAndServe(":8021", nil))
}
