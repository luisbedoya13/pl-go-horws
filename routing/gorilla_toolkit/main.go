package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/articles/{category}/{id:[0-9]+}", articleHandler) // Path based
	r.HandleFunc("/papers/", paperHandler)                           // Query Params based
	server := &http.Server{
		Handler:      r,
		Addr:         "127.0.0.1:8025",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Fatal(server.ListenAndServe())
}

func articleHandler(rw http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	rw.WriteHeader(http.StatusOK)
	fmt.Fprintf(rw, "Category is: %s\nID is: %s", vars["category"], vars["id"])
}

func paperHandler(rw http.ResponseWriter, r *http.Request) {
	queryParams := r.URL.Query()
	rw.WriteHeader(http.StatusOK)
	fmt.Fprintf(rw, "Category is: %s\nID is: %s", queryParams["category"][0], queryParams["id"][0])
}
