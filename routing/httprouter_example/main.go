package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os/exec"

	"github.com/julienschmidt/httprouter"
)

func main() {
	router := httprouter.New()
	router.GET("/api/v1/version", bashVersion)
	router.GET("/api/v1/greet/:name", getFileContent)
	router.ServeFiles("/static/*filepath", http.Dir("./routing/static")) // Dir is relative from bin is executed
	log.Fatal(http.ListenAndServe(":8024", router))
}

func getCommandOutput(command string, args ...string) string {
	out, _ := exec.Command(command, args...).Output()
	return string(out)
}

func bashVersion(rw http.ResponseWriter, r *http.Request, params httprouter.Params) {
	rs := getCommandOutput("/usr/bin/bash", "--version")
	io.WriteString(rw, rs)
}

func getFileContent(rw http.ResponseWriter, r *http.Request, params httprouter.Params) {
	fmt.Fprintf(rw, "Hello %s", params.ByName("name"))
}
