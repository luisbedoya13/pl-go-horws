package main

import (
	"fmt"
	"math/rand"
	"net/http"
)

func main() {
	mux := &CustomMultiplexer{}
	http.ListenAndServe(":8022", mux)
}

type CustomMultiplexer struct{}

func (p *CustomMultiplexer) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/" {
		fmt.Fprint(rw, giveRandomUUID())
		return
	}
}

func giveRandomUUID() string {
	b := make([]byte, 10)
	if _, err := rand.Read(b); err != nil {
		panic(err)
	}
	return fmt.Sprintf("%x", b)
}
