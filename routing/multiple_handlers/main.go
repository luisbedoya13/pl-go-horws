package main

import (
	"fmt"
	"math/rand"
	"net/http"
)

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/float", func(rw http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(rw, rand.Float64())
	})
	mux.HandleFunc("/int", func(rw http.ResponseWriter, r *http.Request) {
		fmt.Fprintln(rw, rand.Intn(100))
	})
	http.ListenAndServe(":8023", mux)
}
