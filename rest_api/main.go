package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"rest_api/mirrors"
	"time"
)

type response struct {
	FastestUrl string        `json:"fastest_url"`
	Latency    time.Duration `json:"latency"`
}

func main() {
	http.HandleFunc("/fastest-mirror", func(rw http.ResponseWriter, r *http.Request) {
		fastestMirror := findFastest(mirrors.MirrorList[:])
		response, _ := json.Marshal(fastestMirror)
		rw.Header().Set("content-type", "application/json")
		rw.Write(response)
	})
	port := ":8011"
	server := &http.Server{
		Addr:           port,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}
	fmt.Printf("fastest mirror api : serving on port %s\n", port)
	log.Fatal(server.ListenAndServe())
}

func findFastest(urls []string) response {
	urlChannel := make(chan string)
	latencyChannel := make(chan time.Duration)
	for _, url := range urls {
		mirrorUrl := url
		go func() {
			start := time.Now()
			_, err := http.Get(mirrorUrl + "/README")
			latency := time.Since(start) / time.Millisecond

			if err == nil {
				urlChannel <- mirrorUrl
				latencyChannel <- latency
			}
		}()
	}
	return response{
		FastestUrl: <-urlChannel,
		Latency:    <-latencyChannel,
	}
}
